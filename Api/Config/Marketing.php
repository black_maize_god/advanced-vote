<?php

namespace Perspective\AdvancedVote\Api\Config;

interface Marketing
{
    public const XML_PATH_PROMO = 'advanced_vote/marketing/promo';

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function isPromoMechanicsAllowed(int $storeId = null): bool;
}
