<?php

namespace Perspective\AdvancedVote\Api\Config;

interface Access
{
    public const XML_PATH_FALSIFICATION = 'advanced_vote/access/falsification';

    public const XML_PATH_SHOW_LISTING = 'advanced_vote/access/show_listing';

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function isFalsificationVotesAllowed(int $storeId = null): bool;

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function isShowVotesListing(int $storeId = null): bool;
}
