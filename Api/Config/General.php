<?php

namespace Perspective\AdvancedVote\Api\Config;

interface General
{
    public const XML_PATH_ENABLED = 'advanced_vote/general/enabled';

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function isEnabled(int $storeId = null): bool;
}
